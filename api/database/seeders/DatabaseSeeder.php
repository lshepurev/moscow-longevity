<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Attend;
use App\Models\Client;
use App\Models\GenderActivity;
use App\Models\Group;
use App\Models\Mapping;
use App\Models\Question;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /** @var string  */
    protected string $dir;

    public function __construct()
    {
        $this->dir = database_path() . '/seeders/data';
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //attends не грузим, не используется

        $this->loadMapping();
        $this->loadActivities();
        $this->loadGroups();
        $this->loadClients();
        $this->loadQuestions();

        GenderActivity::truncate();
        $this->loadGenderActivities('Муж.csv', 'Мужской');
        $this->loadGenderActivities('Жен.csv', 'Женский');
    }

    public function loadMapping(): void
    {
        $handle = fopen($this->dir . '/mapping.csv', "r");
        $columns = ['id','code','name','type','description',];
        $counter = 0;

        while (($data = fgetcsv($handle, null, ',')) !== false) {
            if ($counter++ == 0) {
                continue;
            }

            $combined = array_combine($columns, $data);

            Mapping::updateOrCreate([
                'code' => $combined['code'],
            ], $combined);
        }

        fclose($handle);
    }

    public function loadActivities(): void
    {
        $handle = fopen($this->dir . '/dict.csv', "r");
        $columns = [
            'type',
            'id_level1',
            'level1',
            'mapping_id',
            'answer',
            'physical_limitations',
            'id_level2',
            'level2',
            'id_level3',
            'level3',
            'd_level1',
            'd_level2',
            'd_level3',
        ];
        $counter = 0;

        while (($data = fgetcsv($handle, null, ',')) !== false) {
            if ($counter++ == 0) {
                continue;
            }

            $combined = array_combine($columns, $data);
            /** @var Mapping $mapping */
            $mapping = Mapping::where('code', $combined['mapping_id'])->first();

            $combined['online'] = mb_stripos($combined['level2'] . ' ' . $combined['level3'], 'ОНЛАЙН') !== false;
            $combined['mapping_id'] = $mapping->id ?? null;
            $combined['physical_limitations'] = !empty($combined['physical_limitations']);

            Activity::updateOrCreate([
                'id_level3' => $combined['id_level3'],
                'id_level2' => $combined['id_level2'],
                'id_level1' => $combined['id_level1'],
            ], $combined);
        }

        fclose($handle);
    }

    /**
     *
     */
    public function loadClients(): void
    {
        $handle = fopen($this->dir . '/users.csv', "r");
        $columns = ['n', 'id', 'created_at', 'gender', 'birthdate', 'address', 'fio'];
        $counter = 0;

        while (($data = fgetcsv($handle, null, ',')) !== false) {
            if ($counter++ == 0) {
                continue;
            }

            $combined = array_diff_key(array_combine($columns, $data), ['n' => 1]);

            Client::updateOrCreate([
                'id' => $combined['id']
            ], $combined);
        }

        fclose($handle);
    }

    /**
     * очень много данных
     */
    public function loadAttends(): void
    {
        $handle = fopen($this->dir . '/attend.csv', "r");
        $columns = ['id', 'group_id', 'client_id', 'direction_1', 'direction_2', 'online', 'attend_date', 'attend_start_time', 'attend_end_time'];
        $counter = 0;

        while (($data = fgetcsv($handle, null, ',')) !== false) {
            if ($counter++ == 0) {
                continue;
            }

            $combined = array_combine($columns, $data);
            $combined['online'] = $combined['online'] == 'Да';

            Attend::updateOrCreate([
                'id' => $combined['id']
            ], $combined);
        }

        fclose($handle);
    }

    /**
     *
     */
    public function loadGroups(): void
    {
        $handle = fopen($this->dir . '/groups.csv', "r");
        $columns = ['id', 'direction_1', 'direction_2', 'direction_3', 'address', 'district', 'area', 'schedule_active_periods', 'schedule_close_periods', 'schedule_plan_period'];
        $counter = 0;

        while (($data = fgetcsv($handle, null, ',')) !== false) {
            if ($counter++ == 0) {
                continue;
            }

            $combined = array_combine($columns, $data);
            /** @var Activity $activity */
            $activity = Activity::where('level1', $combined['direction_1'])
                ->where('level2', $combined['direction_2'])
                ->where('level3', $combined['direction_3'])
                ->first();

            $combined['activity_id'] = $activity->id ?? null;

            Group::updateOrCreate([
                'id' => $combined['id']
            ], $combined);
        }

        fclose($handle);
    }

    /**
     *
     */
    public function loadQuestions(): void
    {
        Question::truncate();

        $questions = [
            [
                'id' => 1,
                'type' => 'text',
                'text' => 'Введите Вашу Фамилию Имя и Отчество',
                'description' => null,
                'placeholder' => 'Фамилия Имя Отчетво',
                'answers' => null,
            ],
            [
                'id' => 2,
                'type' => 'date',
                'text' => 'Введите Вашу дату рождения в формате год, месяц, день.',
                'description' => null,
                'placeholder' => 'Дата рождения',
                'answers' => null,
            ],
            [
                'id' => 3,
                'type' => 'text',
                'text' => 'Укажите Ваш адрес проживания',
                'description' => null,
                'placeholder' => 'Адрес проживания',
                'answers' => null,
            ],
            [
                'id' => 4,
                'type' => 'radio',
                'text' => 'Есть ли у Вас заболевания, ограничивающие физическую активность?',
                'description' => null,
                'placeholder' => null,
                'answers' => ['Да', 'Нет'],
            ],
            [
                'id' => 5,
                'type' => 'radio',
                'text' => 'Как Вам было бы удобно заниматься?',
                'description' => null,
                'placeholder' => null,
                'answers' => ['Онлайн (через интернет)', 'Оффлайн (очное занятие)', 'Оба варианта'],
            ],
            [
                'id' => 6,
                'type' => 'checkbox',
                'text' => 'В какое время Вам будет удобно заниматься?',
                'description' => null,
                'placeholder' => null,
                'answers' => [
                    'В промежуток с 8:00 до 10:00',
                    'В промежуток с 11:00 до 13:00',
                    'В промежуток с 14:00 до 16:00',
                    'В промежуток с 17:00 до 19:00',
                ],
            ],
            [
                'id' => 7,
                'type' => 'checkbox',
                'text' => 'Какие программы были бы самыми полезными для развития Ваших навыков и нераскрытого потенциала?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 8,
                'type' => 'checkbox',
                'text' => 'Какое из следующих занятий вызывает у Вас наибольший интерес?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 9,
                'type' => 'checkbox',
                'text' => 'Какие программы, по Вашему мнению, способны привнести больше радости и счастья в Вашу жизнь?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 10,
                'type' => 'checkbox',
                'text' => 'Какие виды деятельности Вам всегда привлекали или вызывали интерес?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 11,
                'type' => 'checkbox',
                'text' => 'Какие программы занятий вызывают у Вас наибольшую страсть или восторг?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 12,
                'type' => 'checkbox',
                'text' => 'Какую программу занятий Вы бы выбрали, чтобы расширить свой кругозор и познать что-то совершенно новое?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
        ];

        foreach ($questions as $question) {
            Question::updateOrCreate([
                'id' => $question['id'],
            ], $question);
        }
    }

    protected function loadGenderActivities(string $filename, string $gender): void
    {
        $handle = fopen($this->dir . '/' . $filename, "r");
        $columns = ['n','age_group','dict_column1'];
        $ageGroupPattern = "/\((-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)\]/ius";
        $matches = [];
        $counter = 0;

        while (($data = fgetcsv($handle, null, ',')) !== false) {
            if ($counter++ == 0) {
                continue;
            }

            $combined = array_combine($columns, $data);
            preg_match($ageGroupPattern, $combined['age_group'], $matches);
            $ids = json_decode($combined['dict_column1'], true);
            foreach ($ids as $id) {
                GenderActivity::create([
                    'id_level3' => $id,
                    'gender' => $gender,
                    'age_min' => (int)($matches[1] - 1),
                    'age_max' => (int)$matches[3],
                ]);
            }
        }
    }
}
