<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Разметка: Для ума/ Для души / Для тела;id_level1;level1;id_level2;level2;id_level3;leve3;d_level1;d_level2;d_level3
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('type')->nullable()->index();
            $table->integer('id_level1')->nullable()->index();
            $table->string('level1')->nullable();
            $table->integer('id_level2')->nullable()->index();
            $table->string('level2')->nullable();
            $table->integer('id_level3')->nullable()->index();
            $table->string('level3')->nullable();
            $table->text('d_level1')->nullable();
            $table->text('d_level2')->nullable();
            $table->text('d_level3')->nullable();
            $table->boolean('online')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
};
