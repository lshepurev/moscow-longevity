<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Group
 *
 * @property int $id
 * @property string $direction_1
 * @property string $direction_2
 * @property string $direction_3
 * @property string $address
 * @property string $district
 * @property string $area
 * @property string $schedule_active_periods
 * @property string $schedule_close_periods
 * @property string $schedule_plan_period
 * @property int $activity_id
 *
 * @property Activity $activity
 *
 * @package App\Models
 */
class Group extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @return HasOne
     */
    public function activity(): HasOne
    {
        return $this->hasOne(Activity::class, 'id', 'activity_id');
    }
}
