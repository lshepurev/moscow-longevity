<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 *
 * @property int $id
 * @property string $gender
 * @property string $birthdate
 * @property string $address
 *
 * @package App\Models
 */
class Client extends Model
{
    use HasFactory;

    protected $guarded = [];
}
