<?php


namespace App\Dto;

/**
 * Class RecSysPredictDto
 *
 * @package App\Dto
 */
class RecSysPredictDto extends BaseDto
{
    protected array $fields = [
        'user_id',
        'group_id',
        'activity_type',
        'n_top',
    ];

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->data['user_id'];
    }

    /**
     * @return string|null
     */
    public function getActivityType(): ?string
    {
        return $this->data['activity_type'] ?? null;
    }

    /**
     * @return int|null
     */
    public function getNTop(): ?int
    {
        return $this->data['n_top'] ?? null;
    }
}
