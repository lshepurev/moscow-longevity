<?php


namespace App\Services;

use App\Dto\RecSysPredictDto;
use App\Helpers\Helper;
use App\Models\Activity;
use App\Models\Attend;
use App\Models\Group;
use Illuminate\Support\Facades\DB;

/**
 * Class AttendService
 *
 * @package App\Services
 */
class AttendService
{
    public function __construct()
    {

    }

    /**
     * Возвращает список мероприятия по фильтру
     *
     * @param array $params
     *
     * @return array
     */
    public function getActivities(array $params): array
    {
        //получаем список всех групп в порядке релевантности
        $groupList = (new RecSysService())->predictGroups(new RecSysPredictDto([
            'user_id' => $params['user_id'] ?? 0,
            'n_top' => 27000,
        ]));

        $groupsLimit = 5;
        //получаем первые n релевантных групп
        $groups = Group::whereIn('id', $groupList['groups_id'])
            ->when($str = ($params['query'] ?? null), function($query) use ($str) {
                return $query->whereRaw('direction_1 || \' \' || direction_2 || \' \' || direction_3 like \'%' . trim($str) . '%\'');
            })
            ->when(is_bool($online = ($params['online'] ?? null)), function($query) use ($online) {
                return $query->where('online', $online);
            })
            ->when($type = ($params['type'] ?? null), function($query) use ($type) {
                return $query->whereHas('activity', function($sub) use ($type) {
                    return $sub->where('type', $type);
                });
            })
            ->where(DB::raw('LENGTH(schedule_active_periods)'), '>', 0)
            ->limit($groupsLimit)
            ->offset($params['offset'] ?? 0)
            ->get();

        $result = [];
        /** @var Group $group */
        foreach ($groups as $group) {
            if (!isset($result[$group->activity->id])) {
                $result[$group->activity->id] = [
                    'type' => $group->activity->type,
                    'address' => $group->activity->online ? '' : $group->address,
                    'title' => $group->activity->level3,
                    'description' => $group->activity->d_level1,
                    'groups' => [],
                ];
            }

            if (count($result[$group->activity->id]['groups']) >= 3) {
                continue;
            }

            $times = Helper::convertStringPeriodsToArrayOfTimes($group->schedule_active_periods);
            $result[$group->activity->id]['groups'][] = [
                'status' => 'Группа занимается',
                'name' => 'G-' . $group->id,
                'times' => $times,
            ];
        }

        return array_values($result);
    }

    /**
     * Возвращает список рекомендованных к посещению мероприятий
     *
     * @param array $params
     *
     * @return array
     */
    public function getRecs(array $params): array
    {
        $recs = (new RecSysService())->predictActivities(new RecSysPredictDto([
            'user_id' => $params['user_id'] ?? 0, 'n_top' => 900
        ]));

        $activities = Activity::whereIn('id_level3', $recs['activities_id'])->get();
        $result = [];

        /** @var Activity $activity */
        foreach ($activities as $activity) {
            if (!isset($result[$activity->type])) {
                $result[$activity->type] = [
                    'title' => $activity->type,
                    'items' => []
                ];
            }

            if (count($result[$activity->type]['items']) >= 5) {
                continue;
            }

            $result[$activity->type]['items'][] = [
                'id' => $activity->id_level3,
                'name' => $activity->level3,
            ];
        }

        return array_values($result);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getRawActivities(array $params): array
    {
        $result = [];
        $recs = (new RecSysService())->predictActivities(new RecSysPredictDto($params));
        $activities = Activity::whereIn('id_level3', $recs['activities_id'])
            ->get()
            ->keyBy('id_level3');

        foreach ($recs['activities_id'] as $id) {
            /** @var Activity $activity */
            $activity = $activities[$id];

            $result[] = [
                'id_level3' => $id,
                'type' => $activity->type,
                'level1' => $activity->level1,
                'level2' => $activity->level2,
                'level3' => $activity->level3,
                'd_level1' => $activity->d_level1,
            ];
        }

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getRawGroups(array $params): array
    {
        $result = [];
        $groupList = (new RecSysService())->predictGroups(new RecSysPredictDto($params));
        $groups = Group::whereIn('id', $groupList['groups_id'])
            ->get()
            ->keyBy('id');

        foreach ($groupList['groups_id'] as $id) {
            /** @var Group $group */
            $group = $groups[$id];

            $result[] = [
                'id' => $id,
                'direction_1' => $group->direction_1,
                'direction_2' => $group->direction_2,
                'direction_3' => $group->direction_3,
                'address' => $group->address,
                'schedule_active_periods' => $group->schedule_active_periods,
            ];
        }

        return $result;
    }

    /**
     * Возвращает список рекомендованных к посещению мероприятий
     *
     * @param array $params
     *
     * @return array
     */
    public function getRecsAlt(array $params): array
    {
        $recs = (new RecSysService())->predictActivities(new RecSysPredictDto([
            'user_id' => $params['user_id'] ?? 0, 'n_top' => 900
        ]));

        $activities = Activity::whereIn('id_level3', $recs['activities_id'])->get();
        $result = [];

        /** @var Activity $activity */
        foreach ($activities as $activity) {
            if (!isset($result[$activity->type])) {
                $result[$activity->type] = [
                    'title' => $activity->type,
                    'items' => []
                ];
            }

            if (count($result[$activity->type]['items']) >= 5) {
                continue;
            }

            $result[$activity->type]['items'][] = [
                'id' => $activity->id_level3,
                'name' => $activity->level3,
            ];
        }

        return array_values($result);
    }
}
