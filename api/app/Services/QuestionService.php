<?php


namespace App\Services;

use App\Models\Question;
use Illuminate\Support\Str;

/**
 * Class QuestionService
 *
 * @package App\Services
 */
class QuestionService
{
    /**
     * Возвращает список вопросов для опроса пользователя
     *
     * @return array
     */
    public function getQuestions(): array
    {
        $result = [];
        $questions = Question::orderBy('id')->get();

        /** @var Question $question */
        foreach ($questions as $question) {
            $result[] = [
                'id' => $question->id,
                'type' => $question->type,
                'title' => $question->text,
                'placeholder' => $question->placeholder,
                'name' => Str::slug($question->text),
                'answers' => $question->answers,
                'load_answers' => $question->load_answers,
            ];
        }

        return $result;
    }
}
