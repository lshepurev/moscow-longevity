<?php


namespace App\Services;

use App\Models\Answer;
use App\Models\Client;
use App\Models\GenderActivity;
use App\Models\Question;
use Carbon\Carbon;

/**
 * Class AnswerService
 *
 * @package App\Services
 */
class AnswerService
{
    /**
     * Должно быть 4 ответа для групп: души, тела, ума и доп (спецпроект или здоровье)
     * Для каждой группы (души, тела, ума и доп)
     * нужно брать топ варианты из предсказаний Сергея и выдавать в виде ответа значение из столбца “Ответы” таблицы
     * Каждый вариант ответа из столбца “Ответ” может выдаваться только для одного вопроса. Если выпадет вариант ответа,
     * который уже был ранее, нужно брать следующее значение из списка предсказаний Сергея для соответствующей группы.
     *
     * @param array $params
     *
     * @return array
     */
    public function getPossibleAnswers(array $params): array
    {
        $prevAnswers = Answer::where('survey_id', $params['survey_id'])->get();

        try {
            $birthdate = Carbon::parse(trim($prevAnswers[1]->answer, "\""));
            $age = $birthdate->age;
        } catch (\Throwable $exception) {
            $birthdate = null;
            $age = null;
        }

        $fio = trim($prevAnswers[0]->answer ?? null, "\"");
        $physicalLimitations = trim($prevAnswers[3]->answer ?? null, "\"") === 'Да';
        $online = null;
        if (!empty($prevAnswers[4]->answer ?? null)) {
            $answer = trim($prevAnswers[4]->answer, "\"");
            if (mb_stripos($answer, 'Онлайн') !== false) {
                $online = true;
            } elseif (mb_stripos($answer, 'Оффлайн') !== false) {
                $online = false;
            }
        }

        $client = null;
        //пробуем авторизовать пользователя
        if (!empty($birthdate) && !empty($fio)) {
            /** @var Client $client */
            $client = Client::where('fio', $fio)
                ->where('birthdate', $birthdate)
                ->first();
        }

        $usedVariants = [];
        /** @var Answer $answer */
        foreach ($prevAnswers as $answer) {
            if ($answer->question_id > 6 && !empty($answer->possible_answers)) {
                $usedVariants = array_merge($usedVariants, $answer->possible_answers);
            }
        }

        //получаем список всех астивностей в порядке релевантности с привязвкой к полу, возрасту и ранее отвеченным ответам
        $builder = GenderActivity::orderBy('id')
            ->when(!empty($client), function($query) use ($client) {
                $gender = mb_stripos($client->gender, 'Жен') !== false ? 'Женский' : 'Мужской';
                return $query->where('gender', $gender);
            })
            ->when(!empty($age), function($query) use ($age) {
                return $query->whereRaw($age . ' >= age_min and ' . $age . ' <= age_max');
            })
            ->whereHas('activity', function($query) use ($online, $physicalLimitations, $usedVariants) {
                $query->when(is_bool($online), function($sub) use ($online) {
                    return $sub->where('online', $online);
                });

                $query->when($physicalLimitations, function($sub) use ($physicalLimitations) {
                    return $sub->where('physical_limitations', $physicalLimitations);
                });

                return $query->whereNotIn('answer', $usedVariants);
            });

        $genderActivities = $builder->get();

        /** @var GenderActivity $genderActivity */
        foreach ($genderActivities as $genderActivity) {
            if (isset($variants[$genderActivity->activity->type])) {
                continue;
            }

            $variants[$genderActivity->activity->type] = $genderActivity->activity->answer;
        }

        $variants[] = 'Ни один из вариантов';

        return array_values($variants);
    }

    /**
     * Сохранение ответа пользователя в БД
     *
     * @param array $params
     *
     * @return array
     */
    public function storeAnswer(array $params): array
    {
        /** @var Question $question */
        $question = Question::find($params['question_id']);

        Answer::updateOrCreate([
            'survey_id' => $params['survey_id'],
            'question_id' => $params['question_id'],
        ], [
            'answer' => $params['answer'] ?? null,
            'possible_answers' => $params['possible_answers'] ?? null,
            'question' => $question->text,
        ]);

        return ['success' => true];
    }
}
