<?php


namespace App\Helpers;

/**
 * Class Helper
 *
 * @package App\Helpers
 */
class Helper
{
    /**
     * @param $query
     *
     * @return string
     */
    public static function getSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }

    /**
     * @param string $schedulePeriod
     *
     * @return array
     */
    public static function convertStringPeriodsToArrayOfTimes(string $schedulePeriod): array
    {
        if (empty($schedulePeriod)) {
            return [];
        }

        $cleanString = preg_replace("/[^a-zA-Zа-яА-Я0-9,:.\s-]/u", "", $schedulePeriod);
        $weekdaysRegex = "/([а-яА-Я]{2,4}\.?)/u";
        preg_match_all($weekdaysRegex, $cleanString, $weekdayMatches);
        $weekdays = array_filter($weekdayMatches[0], function($item) {
            return in_array($item, ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.']);
        });

        $timesRegex = "/(\d{1,2}:\d{1,2}-\d{1,2}:\d{1,2})/u";
        preg_match_all($timesRegex, $cleanString, $timeMatches);
        $times = $timeMatches[0];

        $result = [];
        foreach ($weekdays as $weekday) {
            $result[] = trim($weekday, '.') . ' ' . $times[0];
        }

        return $result;
    }
}
