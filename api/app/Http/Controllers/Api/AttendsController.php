<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\AttendService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class AttendsController
 * @package App\Http\Controllers\Api
 */
class AttendsController extends Controller
{
    /**
     * Возвращает список мероприятий по фильтру
     *
     * @param Request $request
     * @param AttendService $attendService
     *
     * @return JsonResponse
     */
    public function getActivities(Request $request, AttendService $attendService): JsonResponse
    {
        $params = $request->all();

        try {
            $result = $attendService->getActivities($params);

        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

    /**
     * Возвращает список мероприятий как выдает модель
     *
     * @param Request $request
     * @param AttendService $attendService
     *
     * @return JsonResponse
     */
    public function getRawActivities(Request $request, AttendService $attendService): JsonResponse
    {
        $request->validate([
            'user_id' => ['required', 'integer'],
        ]);

        $params = $request->all();

        try {
            $result = $attendService->getRawActivities($params);

        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

    /**
     * Возвращает список групп как выдает модель
     *
     * @param Request $request
     * @param AttendService $attendService
     *
     * @return JsonResponse
     */
    public function getRawGroups(Request $request, AttendService $attendService): JsonResponse
    {
        $request->validate([
            'user_id' => ['required', 'integer'],
        ]);

        $params = $request->all();

        try {
            $result = $attendService->getRawGroups($params);

        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

    /**
     * Возвращает список рекомендаций к посещению
     *
     * @param Request $request
     * @param AttendService $attendService
     *
     * @return JsonResponse
     */
    public function getRecommends(Request $request, AttendService $attendService): JsonResponse
    {
        $params = $request->all();

        try {
            $result = $attendService->getRecs($params);
        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

    /**
     * Возвращает список рекомендаций к посещению
     *
     * @param Request $request
     * @param AttendService $attendService
     *
     * @return JsonResponse
     */
    public function getRecommendsAlt(Request $request, AttendService $attendService): JsonResponse
    {
        $params = $request->all();

        try {
            $result = $attendService->getRecs($params);
        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

}
