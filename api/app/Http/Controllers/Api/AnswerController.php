<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class AnswerController
 *
 * @package App\Http\Controllers\Api
 */
class AnswerController extends Controller
{
    /**
     * Возвращает варинаты отватов на вопрос
     *
     * @param Request $request
     * @param AnswerService $answerService
     *
     * @return JsonResponse
     */
    public function getPossibleAnswers(Request $request, AnswerService $answerService): JsonResponse
    {
        $request->validate([
            'survey_id' => ['required', 'integer'],
            'question_id' => ['required', 'integer'],
        ]);

        $params = $request->all();

        try {
            $result = $answerService->getPossibleAnswers($params);
        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

    /**
     * Сохраняет ответ пользователя в БД
     *
     * @param Request $request
     * @param AnswerService $answerService
     *
     * @return JsonResponse
     */
    public function storeAnswer(Request $request, AnswerService $answerService): JsonResponse
    {
        $request->validate([
            'survey_id' => ['required', 'integer'],
            'question_id' => ['required', 'integer'],
            'possible_answers' => ['array', 'nullable'],
            'answer' => ['string'],
        ]);

        $params = $request->all();

        try {
            $result = $answerService->storeAnswer($params);
        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }
}
