<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class QuestionsController
 *
 * @package App\Http\Controllers\Api
 */
class QuestionsController extends Controller
{
    /**
     * @param Request $request
     * @param QuestionService $questionService
     *
     * @return JsonResponse
     */
    public function getQuestions(Request $request, QuestionService $questionService): JsonResponse
    {
        $params = $request->all();

        try {
            $result = $questionService->getQuestions();

        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }
}
