import React, {Component, Fragment} from 'react'
import {Routes, Route, useParams, useLocation} from 'react-router-dom'
import {connect} from 'react-redux'
import {changeMedia} from "../actions/main";
import Main from "../routes";

class Full extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const {changeMedia} = this.props

        const mediaQuery = window.matchMedia('(min-width: 768px)')

        if (mediaQuery.matches) {
            changeMedia(false)
        } else {
            changeMedia(true)
        }

        mediaQuery.addListener((mq) => {
            if (mq.matches) {
                changeMedia(false)
            } else {
                changeMedia(true)
            }
        })

        this.props.history.listen((location) => {
            if (location.action === 'POP')
                window.location.reload()
        })

        window.scrollTo(0, 0)
    }

    render() {
        const Wrapper = (props) => {
            const params = useParams()
            const loc = useLocation()

            return <Main {...{...props, match: {params}, location: {loc}}} />
        }

        return (
            <Fragment>
                <Routes>
                    <Route path={'*'} index element={<Wrapper/>} />
                </Routes>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = {
    changeMedia
}


export default connect(mapStateToProps, mapDispatchToProps)(Full)
