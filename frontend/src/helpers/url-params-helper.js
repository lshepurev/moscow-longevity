export const getURLSearchParams = () => {
    const search = new URLSearchParams(window.location.search)

    let queryParams = {}
    for (let item of search) {
        queryParams[item[0]] = item[1]
    }

    return queryParams
}
