import {requestApi} from "../api/api";
import {changeLoader, changeError, change404} from "./main";

export const GET_RECOMMENDS_SUCCESS = 'GET_RECOMMENDS_SUCCESS';
export const GET_QUESTIONS_SUCCESS = 'GET_QUESTIONS_SUCCESS';

export const getRecommendsSuccess = (data) => ({
    type: GET_RECOMMENDS_SUCCESS,
    data: data,
})

export const getQuestionsSuccess = (data) => ({
    type: GET_QUESTIONS_SUCCESS,
    data: data,
})

/**
 * Возвращает список рекомендаций
 *
 * @param params
 *
 * @returns {(function(*): Promise<*|null|undefined>)|*}
 */
export const getRecommends = (params) => async (dispatch) => {
    try {
        dispatch(changeLoader(true))
        const result = await requestApi('get', '/recommends', params)

        return result
    } catch (e) {
        return null
    } finally {
        dispatch(changeLoader(false))
    }
}

/**
 * Возвращает список активностей по фильтрам
 *
 * @param params
 *
 * @returns {(function(*): Promise<*>)|*}
 */
export const getActivities = (params) => async (dispatch) => {
    try {
        dispatch(changeLoader(true))
        const result = await requestApi('get', '/activities', params)

        return result
    } catch (e) {
        return null
    } finally {
        dispatch(changeLoader(false))
    }
}

/**
 * Возвращает список вопросов
 *
 * @param params
 *
 * @returns {(function(*): Promise<*|null|undefined>)|*}
 */
export const getQuestions = (params) => async (dispatch) => {
    try {
        dispatch(changeLoader(true))
        const result = await requestApi('get', '/questions', params)

        return result
    } catch (e) {
        return null
    } finally {
        dispatch(changeLoader(false))
    }
}

/**
 * Создает новый опрос
 *
 * @param params
 *
 * @returns {(function(*): Promise<*|null|undefined>)|*}
 */
export const createSurvey = (params) => async (dispatch) => {
    try {
        dispatch(changeLoader(true))
        const result = await requestApi('post', '/survey', params)

        return result
    } catch (e) {
        return null
    } finally {
        dispatch(changeLoader(false))
    }
}

/**
 * Завершение опроса
 *
 * @param params
 *
 * @returns {(function(*): Promise<*|null|undefined>)|*}
 */
export const updateSurvey = (params) => async (dispatch) => {
    try {
        dispatch(changeLoader(true))
        const result = await requestApi('post', '/survey/update', params)

        return result
    } catch (e) {
        return null
    } finally {
        dispatch(changeLoader(false))
    }
}

/**
 * Возвращает возможные варианты ответов
 *
 * @param params
 *
 * @returns {(function(*): Promise<*|null|undefined>)|*}
 */
export const getPossibleAnswers = (params) => async (dispatch) => {
    try {
        dispatch(changeLoader(true))
        const result = await requestApi('get', '/possible-answers', params)

        return result
    } catch (e) {
        return null
    } finally {
        dispatch(changeLoader(false))
    }
}

/**
 * Сохраняет ответ пользователя
 *
 * @param params
 *
 * @returns {(function(*): Promise<*|null|undefined>)|*}
 */
export const storeAnswer = (params) => async (dispatch) => {
    try {
        dispatch(changeLoader(true))
        const result = await requestApi('post', '/answer', params)

        return result
    } catch (e) {
        return null
    } finally {
        dispatch(changeLoader(false))
    }
}