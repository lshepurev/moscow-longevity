export const CHANGE_SLUG = 'CHANGE_SLUG'
export const CHANGE_MEDIA = 'CHANGE_MEDIA'
export const CHANGE_LOADER = 'CHANGE_LOADER'
export const CHANGE_ERROR = 'CHANGE_ERROR'
export const CHANGE_404 = 'CHANGE_404'

/**
 * Action для смены типа события сбора статистики
 *
 * @param value
 */
export const changeSlug = (value) => ({
    type: CHANGE_SLUG,
    value: value
})


/**
 * Action для смены типа сайта
 *
 * @returns {function(*)}
 */
export const changeMedia = (value) => (dispatch) => {
    dispatch({
        type: CHANGE_MEDIA,
        value: value
    })
}

/**
 * Action для смены статуса загрузки данных
 *
 * @returns {function(*)}
 */
export const changeLoader = (value) => (dispatch) => {
    dispatch({
        type: CHANGE_LOADER,
        value: value
    })
}
/**
 * Action для закрытия сообщения об ошибке
 *
 * @param value
 *
 * @returns {{type: string, value}}
 */
export const changeError = (value) => ({
    type: CHANGE_ERROR,
    value: value
})
/**
 * Action для случая, когда опрос не найден
 *
 * @param value
 *
 * @returns {{}}
 */
export const change404 = (value) => ({
    type: CHANGE_404,
    value: value
})
