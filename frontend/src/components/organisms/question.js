import React from "react";
import styled from "styled-components";
import {Container} from "../atoms/container";
import {CheckboxWithLabel} from "../atoms/checkbox-with-label";
import {Text} from "../atoms/text";
import {RadioText} from "../atoms/radio-text";
import {RadioButton} from "../atoms/radio-button";
import {InputDate} from "../molecules/date";
import {Input} from "../atoms/input";
import {StyledInputWithMask} from "../atoms/styled-input-with-mask";

const Title = styled(Text)`
   
`

export const Question = (props) => {
    const {question, mobile, answers, storeAnswer} = props;

    const onSelectHandler = (id, item, type) => {
        switch (type) {
            case 'checkbox':
                let selectedItems = answers[question.id] ? answers[question.id] : [];
                if (selectedItems.includes(item)) {
                    storeAnswer(question, selectedItems.filter(i => i !== item), item);
                } else {
                    storeAnswer(question, [...selectedItems, item], item);
                }
                break;
            default:
                storeAnswer(question, item, item)
                break;
        }
    }

    const getCheckbox = () => {
        return question.answers && question.answers.map((item, i) => <Container key={'question-answer-' + i}>
            <CheckboxWithLabel
                label={item}
                name={question.name}
                checked={answers[question.id] && answers[question.id].includes(item)}
                onClick={() => onSelectHandler(question.id, item, 'checkbox')}
                mobile={mobile}
            />
        </Container>)
    }

    const getRadio = () => {
        return <Container gap={12} flow={mobile ? 'column' : 'row'}>{question.answers.map((item, i) => {
            return <RadioButton
                key={'r_' + '_' + i}
                height={'44px'}
                align={'center'}
                padding={'4px'}
                weight={400}
                isSelected={answers[question.id] === item}
                onClick={() => onSelectHandler(question.id, item, question.type)}
            >
                <RadioText
                    align={'center'}
                    color={'#008844'}
                    isSelected={answers[question.id] === item}
                >{item}</RadioText>
            </RadioButton>
        })}</Container>
    }

    const getText = () => {
        return <Container flow={'column'} gap={8}>
            <Input
                type={'text'}
                placeholder={question.placeholder}
                value={answers[question.id] || ''}
                onChange={(e) => onSelectHandler(question.id, e.currentTarget.value)}
            />
        </Container>
    }

    const getDate = () => {
        return <Container flow={'column'} gap={8}>
            <StyledInputWithMask
                width={'120px'}
                padding={'0px 12px'}
                height={'24px'}
                mask={'aa.aa.aaaa'}
                formatChars={{'a': '[0-9]'}}
                maskChar={''}
                placeholder={'01.01.1956'}
                value={answers[question.id] || ''}
                selected={answers[question.id] || ''}
                onChange={(el) => onSelectHandler(question.id, el.currentTarget.value, question.type)}
            />
        </Container>
    }

    return <Container shrink={'0'} flow={'column'}>
        <Container flow={'column'}>
            <Title>{question.title}</Title>
        </Container>
        <Container flow={'column'} margin={'18px 0 0 0'}>
            {question.type === 'checkbox' && getCheckbox()}
            {question.type === 'radio' && getRadio()}
            {question.type === 'text' && getText()}
            {question.type === 'date' && getDate()}
            </Container>
    </Container>
}