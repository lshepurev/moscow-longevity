import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'
import {Container} from "../atoms/container";
import {Logo} from "../atoms/logo";
import {Text} from "../atoms/text";

const BaseContainer = styled(Container)`
  position: relative;
  min-height: 100vh;

  @media (max-width: 767px) {
    min-height: 100%;
  }
`

const ContentContainer = styled(Container)`
  position: relative;
  overflow-y: auto;
  overflow-x: hidden;
  height: 100%;
  min-height: 100vh;

  margin: 0 auto;
  width: 76vw;

  @media (max-width: 767px) {
    width: 100%;
    min-height: 100%;
  }
`

const Banner = styled(Container)`
    background-position: center center;
    height: 320px;
    width: 100%;
    background-color: #007339;
    z-index: 100;
`

const TitleContainer = styled(Container)`
    
`

const Title = styled(Text)`
    max-width: 50%;
    color: #ffffff;
    font-size: 70px;
    font-family: 'Golos-Text',Arial,sans-serif;
    line-height: 1;
    font-weight: 700;
    background-position: center center;
    border-color: transparent;
    border-style: solid;
    line-height: 70px;
`

const SubTitle = styled(Text)`
    max-width: 50%;
    margin-top: 16px;
    color: #ffffff;
    font-size: 18px;
    font-family: 'Golos-Text',Arial,sans-serif;
    line-height: 1.55;
    font-weight: 400;
    background-position: center center;
    border-color: transparent;
    border-style: solid;
    line-height: 37px;
`

class BaseTemplatePure extends PureComponent {
    constructor(props) {
        super(props)
    }

    render() {
        const {children, mobile} = this.props
        return (
            <BaseContainer flow={'column'} align={'stretch'} shrink={'0'} justify={mobile ? 'flex-start' : 'center'}>
                <Banner flow={'row'}>
                    <TitleContainer flow={'column'} justify={mobile ? 'flex-start' : 'center'} align={'center'}>
                        <Title>«Московское долголетие»</Title>
                        <SubTitle>Проект Мэра Москвы для активных москвичей старшего поколения</SubTitle>
                    </TitleContainer>
                    <Container width={'100%'} justify={'right'}>
                        {!mobile && <Logo src={'/img/logo.png'} alt={''} />}
                    </Container>
                </Banner>
                <ContentContainer shrink={'0'}>
                    {children}
                </ContentContainer>
            </BaseContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
    }
}

export const BaseTemplate = connect(mapStateToProps, null)(BaseTemplatePure)
