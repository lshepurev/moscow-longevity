import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "../atoms/container";
import {MenuItemTitle} from "../atoms/menu-item-title";
import {Toggle} from "../atoms/toggle";

export const Format = () => {
    return <Container flow={"column"}>
        <MenuItemTitle>Формат</MenuItemTitle>
        <Container margin={'14px 0 0 0'}>
            <Toggle />
        </Container>
    </Container>
}