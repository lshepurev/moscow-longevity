const css = String.raw

export const color = {
    green: '#79B821',
    grayIcon: '#878787',
    grayIconHover: '#aaaaaa',
    grayLighter: '#f0f4f5',
    black: '#000000',
    white: '#ffffff',
    gray: '#999',
    grey: '#d8d8d8',
    default: '#dcdcdc',
    primary: '#0f4fa8',
    success: '#09a275',
    info: '#b10058',
    warning: '#f2b701',
    danger: '#dc0030',
    hoveredBox: 'rgba(33, 33, 33, 0.05)',
    blue: '#ADC3D3',
    darkBlue: '#192F59',
    lightBlue: '#f0f8ff',
    lightGreen: '#f0fff0',
    lightRed: '#ffe4e1',
    hoverRed: '#ec5858',
    purple: '#910cc7',
    orangeRed: '#ff6600',
    orange: '#ffa500',
    text: '#0C1014',
    attention: '#FFEFD3',
    lead0: 'rgba(217,241,221,0.8)',
    lead1: '#D9F1DD',
    lead2: '#449837',
    coal3: '#DCDCDC',
    coal1: '#F7F7F7',
    accent3: '#CC2944',
    darkGreen: '#006400',
}

export const tagColor = {
    red: '#ff0000',
    lawnGreen: '#7fff00',
    pink: '#ff1493',
    aquamarine: '#66cdaa',
    orange: '#ffa500',
    aqua: '#00ffff',
    yellow: '#ffff00',
    blue: '#ADC3D3',
    fuchsia: '#ff00ff',
    brown: '#8b4513',
    black: '#000000',
    grey: '#a9a9a9',
    pastelPink: '#f0a9a9',
    purple: '#800080',
    darkBlue: '#00008b',
    steelBlue: '#4682b4',
    orangeRed: '#FF6F00FF',
    navy: '#000080',
    violetRed: '#c71585',
    darkGreen: '#006400',
    darkRed: '#8b0000',
    default: '#dcdcdc',
    primary: '#0f4fa8',
    success: '#09a275',
    info: '#b10058',
    warning: '#f2b701',
    danger: '#dc0030',
}

export const buttonColor = {
    default: '#212121',
    primary: '#0f4fa8',
    success: '#09a275',
    info: '#b10058',
    warning: '#f2b701',
    danger: '#dc0030',
    blue: '#4a90e2',
    orange: '#ffa500',
}

export const pauseColor = {
    success: 'rgba(9, 162, 117, 0.2)',
}

export const hoverColor = {
    default: 'rgba(220, 220, 220, 0.5)',
    info: 'rgba(177, 0, 88, 0.85)',
    danger: 'rgba(220, 0, 48, 0.85)',
    primary: 'rgba(15, 79, 168, 0.85)',
    warning: 'rgba(242, 183, 1, 0.85)',
    success: 'rgba(9, 162, 117, 0.85)'
}

export const hoverBorder = {
    default: 'rgba(220, 220, 220, 0.1)',
    info: 'rgba(177, 0, 88, 0.1)',
    danger: 'rgba(220, 0, 48, 0.1)',
    primary: 'rgba(15, 79, 168, 0.1)',
    warning: 'rgba(242, 183, 1, 0.1)',
    success: 'rgba(9, 162, 117, 0.1)'
}

export const outlineBorder = {
    default: 'rgba(220, 220, 220, 0.5)',
    info: 'rgba(177, 0, 88, 0.5)',
    danger: 'rgba(220, 0, 48, 0.5)',
    primary: 'rgba(15, 79, 168, 0.5)',
    warning: 'rgba(242, 183, 1, 0.5)',
    success: 'rgba(9, 162, 117, 0.5)'
}

export const badgeColors = {
    default: '#dcdcdc',
    primary: '#0f4fa8',
    success: '#09a275',
    info: '#b10058',
    warning: '#f2b701',
    danger: '#dc0030',
    white: '#ffffff',
}

export const font = {
    pt: 'Gilroy, Tahoma, sans-serif',
    textSizeDefault: '14px',
    textColorDefault: color.black,
}

export const transition ='transition: all 600ms cubic-bezier(0.23, 1, 0.32, 1)'
export const noTextSelection = `
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  -o-user-select: none;
  user-select: none;
`

export const formControl = `
  border-radius: 0;
  box-shadow: none;
  color: #0c1014;
  height: 42px;
`