import React from "react";
import styled from "styled-components";
import {InputWithMask} from "./input-with-mask";

export const StyledInputWithMask = styled(InputWithMask)`
  display: block;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: transparent;
  border: none;
  border-bottom: 1px solid #ADC3D3;
  background-image: none;
  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  outline: none;
  padding: 6px 12px;

  &::-webkit-input-placeholder {
    color: #adc3d3;
  }
`
