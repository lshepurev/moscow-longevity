import React, { Component }  from 'react'
import styled, { keyframes } from 'styled-components'

const rotate = keyframes`
  100% {
    transform: rotate(360deg);
  }
`

const dash = keyframes`
  0% {
    stroke-dasharray: 1, 200;
    stroke-dashoffset: 0;
  }
  50% {
    stroke-dasharray: 89, 200;
    stroke-dashoffset: -35px;
  }
  100% {
    stroke-dasharray: 89, 200;
    stroke-dashoffset: -124px;
  }
`

const LoaderBg = styled.div`
  position: absolute;
  padding: 5%;
  z-index: 101;
  background: rgba(255, 255, 255, 0.4);
  border-radius: inherit;
`

const LoaderDiv = styled.div`
  position: relative;
  width: 60px;
  &:before {
    content: '';
    display: block;
    padding-top: 100%;
  }
`

const LoaderSvg = styled.svg`
  animation: ${rotate} 2s linear infinite;
  height: 100%;
  transform-origin: center center;
  width: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  & circle {
    stroke-dasharray: 2, 200;
    stroke-dashoffset: 0;
    animation: ${dash} 1.5s ease-in-out infinite;
    stroke-linecap: round;
    stroke: #e7ebef;
  }
`

export const ContentLoader = (props) => {
  return (
    <LoaderBg>
      <LoaderDiv>
        <LoaderSvg className="loader">
          <svg viewBox={'25 25 50 50'}>
            <circle cx={'50'}
              cy={'50'}
              r={'20'}
              fill="none"
              strokeWidth="1"
              strokeMiterlimit="10"
            />
          </svg>
        </LoaderSvg>
      </LoaderDiv>
    </LoaderBg>
  )
}
