import React from 'react'
import styled, {css} from 'styled-components'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'
import {color} from "../theme";

//Подняли наверх чтобы в omit можно было указать propTypes
export const InputWithMask = React.forwardRef((props, ref) => {
  return props.mask ? <MaskedInput inputRef={ref} {...props}/> : <BaseInput ref={ref} {...props} />
})

//В отдельной переменной потому что в продакшн среде propTypes будут отрезаны из билда
const InputPropTypes = {
  borderColor: PropTypes.string,
  wide: PropTypes.bool,
  align: PropTypes.string,
  padding: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  hideBorder: PropTypes.bool,
  color: PropTypes.string,
  lineHeight: PropTypes.string,
  caretColor: PropTypes.string,
}
InputWithMask.propTypes = InputPropTypes

InputWithMask.defaultProps = {
  wide: false,
  hideBorder: false,
  padding: '0',
  align: 'left',
  width: '100%',
  height: '24px',
  borderColor: 'blanco2',
  color: '#000',
  lineHeight: '24px',
  caretColor: '1st_4'
}


export const BaseInput = styled.input`
  display: flex;
  line-height: ${p => p.lineHeight};
  color: ${(p) => color[p.color]};
  background: transparent;
  border: none;
  caret-color: ${(p) => color[p.caretColor]};
  border: 2px solid #3DCFCF;
  width: ${(p) => p.width};

  text-align: ${p => p.align};

  ${(p) => p.size && css`
    font-size: ${p.size};
  `}
  ${(p) => p.borderRadius && css`
    border-radius: ${p.borderRadius};
  `}
  ${(p) => p.padding && css`
    padding: ${p.padding};
  `}
  ${(p) => p.height && css`
    height: ${p.height};
  `}
  ${(p) => p.wide && css`
    width: 100%;
  `}
  ${(p) => p.hideBorder && css`
    border: none;
  `}

  &:focus, &:active {
    outline: none;
  }

  &::-webkit-input-placeholder {
    color: rgba(0,0,0,0.2);
  }

  &::-moz-placeholder {
    color: rgba(0,0,0,0.2);
  }
  &:-moz-placeholder {
    color: rgba(0,0,0,0.2);
  }
  &:-ms-input-placeholder {
    color: rgba(0,0,0,0.2);
  }
`

const MaskedInput = BaseInput.withComponent(props => {
  return <InputMask {...props}/>
})

