import styled from "styled-components";
import {Text} from "./text";

export const StyledLink = styled(Text)`
    &:link {
        color: blue;
    }

    &:visited {
        color: green;
    }

    &:hover {
        color: hotpink;
    }

    &:active {
        color: blue;
    }
`

export const LinkText = styled(Text)`
    cursor: pointer;
    color: blue;

    &:link {
        color: blue;
    }

    &:visited {
        color: green;
    }

    &:hover {
        color: hotpink;
    }

    &:active {
        color: blue;
    }
`
