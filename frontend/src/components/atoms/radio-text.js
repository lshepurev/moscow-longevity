import styled, {css} from "styled-components";
import {color} from "../theme";
import {Text} from './text'

export const RadioText = styled(Text)`
   font-size: ${p => p.size ? p.size : '13px'};
   color:  ${p => p.color ? p.color : '#4D9898'};

   ${p => p.isSelected && css`
       color:  ${color['white']};
   `}
`
