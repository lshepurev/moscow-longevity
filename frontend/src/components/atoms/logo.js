import styled, {css} from 'styled-components'
import PropTypes from 'prop-types'

export const Logo = styled.img`
  max-height:100%;
  width: ${(p) => p.width};
  height: ${(p) => p.height};
  overflow: hidden;
  
  ${(p) => p.borderRadius && css`
     border-radius: ${p.borderRadius};  
  `}
 
  ${(p) => p.shrink && css`
     flex-shrink: ${p.shrink};
   `}
`

Logo.propTypes = {
    radius: PropTypes.string,
    shrink: PropTypes.string,
}

Logo.defaultProps = {
    radius: '48px'
}