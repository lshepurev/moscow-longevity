import styled, {css} from "styled-components";
import {Container} from "./container";

export const RadioButton = styled(Container)`
  cursor: pointer;
  border-radius: 4px;
  transition: color, background 0.2s linear;
  background-color: ${p => p.isDisabled ? '#f0f9f9' : '#D7F0F0'};
  color: ${p => p.isDisabled ? '#a6cece' : '#4D9898'};
  padding: ${p => p.padding ? p.padding : '0px'};
  font-weight: ${p => p.weight ? p.weight : 300};
  font-size: 17px;
  line-height: 24px;

  &:hover, &:active {
    background-color: ${p => p.isDisabled ? '#f0f9f9' : '#84D1D1'};
    color: ${p => p.isDisabled ? '#a6cece' : '#084D4D'};
  }

  ${p => p.isSelected && css`
    background-color: ${p => p.isDisabled ? '#f0f9f9' : '#84D1D1'};
    color: ${p => p.isDisabled ? '#a6cece' : '#084D4D'};
  `}
`
