import {combineReducers} from 'redux'
import {main} from "./main"
import {connectRouter} from "connected-react-router";

const reducers = {main: main}

const rootReducer = (history) => combineReducers({
    router: connectRouter(history),
    ...reducers
})

export default rootReducer
