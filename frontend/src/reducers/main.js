import {CHANGE_LOADER, CHANGE_MEDIA, CHANGE_404, CHANGE_SLUG} from "../actions/main";

export const initialState = {
    mobile: false,
    isLoading: false,
    isError: false,
    is404: null,
}

export const main = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_SLUG:
            return {
                ...state,
                slug: action.value,
            }
        case CHANGE_MEDIA:
            return {
                ...state,
                mobile: action.value,
            }
        case CHANGE_LOADER:
            return {
                ...state,
                isLoading: action.value,
            }
        case CHANGE_404:
            return {
                ...state,
                is404: action.value,
            }
        default:
            return state
    }
}
