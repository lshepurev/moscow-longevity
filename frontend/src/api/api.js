import axios            from 'axios'
import Qs               from 'qs'
import Cookies          from 'js-cookie'
import MockAdapter from "axios-mock-adapter";

export const API_URL = process.env.API_URL
export const WITH_MOCKS = process.env.WITH_MOCKS

const TIMEOUT = 45000

const axiosInstance = axios.create({
  baseURL: API_URL,
  withCredentials: false,
  paramsSerializer: function(params) {
    return Qs.stringify(params, {arrayFormat: 'brackets'})
  }
})

export {axiosInstance}

export const requestApi = async (method, url, data, multipart = false) => {
  const config = {
    method: method,
    url: url,
    params: {},
    timeout: TIMEOUT,
  }
  if (data) {
    switch (method) {
      case 'post':
      case 'put':
      case 'patch':
        config.data = data
        break
      default:
        config.params = {...config.params, ...data}
        break
    }
  }

  if (multipart) {
    const formData = new FormData()
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        formData.append(key, data[key])
      }
    }
    config.headers = {...config.headers, 'Content-Type': undefined, enctype: 'multipart/form-data'}
    config.data = formData
  } else if (method !== 'get') {
    config.data = data
  }

  try {
    let response = await axiosInstance.request(config)
    return response.data
  } catch (error) {
    throw (error)
  }
}
