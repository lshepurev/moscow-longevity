import React, {Component, useEffect, useState} from "react";
import styled from "styled-components";
import {getURLSearchParams} from "../helpers/url-params-helper";
import {Container} from "../components/atoms/container";
import {Question} from "../components/organisms/question";
import {Button} from "../components/atoms/button";
import {Text} from "../components/atoms/text";
import {LinkText} from "../components/atoms/link-text";
import {getQuestions, createSurvey, storeAnswer, updateSurvey, getPossibleAnswers} from "../actions/service";
import {push} from "connected-react-router";
import {connect} from "react-redux";

const StyledContainer = styled(Container)`
    background-color: #e7ebef;
    padding: 24px;
`

const QuestionContainer = styled(Container)`
    background-color: #fff;
    padding: 24px;
    max-width: 620px;
`

const STATUS_SURVEY_NEW = 1;
const STATUS_SURVEY_FINISHED = 2;

class QuestionsComponent extends Component {
    constructor(props) {
        super(props);

        const params = getURLSearchParams();

        this.state = {
            survey_id: null,
            current: params.current ? Number(params.current) : 0,
            answers: {},
            questions: [],
        };
    }

    async componentDidMount() {
        const survey = await this.props.createSurvey();
        const questions = await this.props.getQuestions();

        this.setState({
            questions: questions,
            survey_id: survey.id,
        })
    }

    /**
     * Следующий вопрос
     *
     * @returns {Promise<void>}
     */
    nextQuestion = async () => {
        const {current, survey_id, answers, questions} = this.state;

        await this.props.storeAnswer({
            survey_id: survey_id,
            answer: JSON.stringify(answers[questions[current].id]),
            question_id: questions[current].id,
            possible_answers: questions[current].answers,
        });

        const next = current + 1;
        if (questions[next] && !!questions[next].load_answers) {
            questions[next].answers = await this.props.getPossibleAnswers({
                question_id: questions[next].id,
                survey_id: survey_id,
            });
        }

        this.setState({
            current: next,
        })
    }

    /**
     * Предыдущий вопрос
     */
    prevQuestion = () => {
        const {current} = this.state;

        this.setState({
            current: current - 1,
        })
    }

    /**
     * Сохранение ответа в БД
     *
     * @param question
     * @param selectedItems
     * @param item
     */
    storeAnswer = (question, selectedItems, item) => {
        const {answers} = this.state;

        this.setState({
            answers: {...answers, [question.id]: selectedItems}
        })
    }

    /**
     * Завершение опроса
     *
     * @returns {Promise<void>}
     */
    finishQuestions = async () => {
        const {survey_id} = this.state;

        await this.props.updateSurvey({
            survey_id: survey_id,
            status_id: STATUS_SURVEY_FINISHED,
        })

        this.props.changeLocation('/list?survey_id=' + survey_id);
    }

    render() {
        const {questions, current, answers, survey_id} = this.state;

        return <Container flow={'column'}>
            <Container>
                <LinkText onClick={() => this.props.changeLocation('/')}>&#8592; Назад</LinkText>
            </Container>
            <StyledContainer flow={'column'} margin={'18px 0 0 0'}>
                {questions.length > 0 && <QuestionContainer flow={'column'}>
                    <Container>
                        <Text>{`Шаг ${current + 1} из ${questions.length + 1}`}</Text>
                    </Container>
                    <Container margin={'16px 0 0 0'}>
                        <Question
                            survey_id={survey_id}
                            mobile={this.props.mobile}
                            question={questions[current]}
                            answers={answers}
                            storeAnswer={this.storeAnswer}
                        />
                    </Container>
                    <Container margin={'16px 0 0 0'} justify={'flex-start'}>
                        {current !== 0 && <Button onClick={this.prevQuestion} margin={'0 16px 0 0'}>Назад</Button>}
                        {current !== questions.length - 1 ? <Button onClick={this.nextQuestion}>Далее</Button> : <Button onClick={this.finishQuestions}>Отправить</Button>}
                    </Container>
                </QuestionContainer>}
            </StyledContainer>
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
    }
}

const mapDispatchToProps = {
    push,
    getQuestions,
    createSurvey,
    storeAnswer,
    updateSurvey,
    getPossibleAnswers,
}

export const Questions = connect(mapStateToProps, mapDispatchToProps)(QuestionsComponent)