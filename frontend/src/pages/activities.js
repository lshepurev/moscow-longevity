import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "../components/atoms/container";
import {Text} from "../components/atoms/text";
import {Button, GreenButton} from "../components/atoms/button";
import {LinkText} from "../components/atoms/link-text";
import {push} from "connected-react-router";
import {connect} from "react-redux";
import {Input} from "../components/atoms/input";
import {getActivities} from "../actions/service";
import {getURLSearchParams} from "../helpers/url-params-helper";

const StyledContainer = styled(Container)`
    background-color: #e7ebef;
    padding: 24px;
`

const ItemContainer = styled(Container)`
    background-color: #fff;
    padding: 24px;
`

const Title = styled(Text)`
    margin-top: 6px;
    font-weight: 600;
    font-size: 26px;
    line-height: 32px;
`

const SubTitle = styled(Text)`
    font-size: 16px;
    line-height: 24px;
    color: #696c71;
`

const Description = styled(Text)`
    line-height: 1.4;
    margin-top: 12px;
`

const GroupList = styled(Container)`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    margin-top: 24px;
    width: 100%;
`

const GroupItem = styled(Container)`
    width: calc(50% - 12px);
    margin-right: 24px
`

const GroupStatus = styled(Text)`
    color: #084;
    margin-bottom: 8px;
    font-weight: 500;
    white-space: nowrap;
    
`

const GroupId = styled(Text)`
    font-weight: 500;
    font-size: 18px;
    line-height: 22px;
    margin-bottom: 8px;
    width: auto;
    margin-right: 4px;
`

const GroupIdNumber = styled(GroupId)`
    color: #717479;
    white-space: nowrap;
`

const Separator = styled.div`
    width: 100%;
    border-bottom: 1px solid #dfe1e6;
    padding-bottom: 40px;
`

const ShowMoreButton = styled(Button)`
    width: 100%;
    background-color: #fff;
    color: #000;
    
    font-weight: 600;
    border-radius: 0.25rem;
    
    &:hover {
        background: #f3f3f3;
    }
`

class SearchComponent extends Component {
    constructor(props) {
        super(props);

        const {survey_id} = getURLSearchParams();

        this.state = {
            filters: {
                survey_id: survey_id,
                query: '',
                type: null,
                online: null,
                offset: 0,
            },
            items: []
        };
    }

    async componentDidMount() {
        await this.searchActivities()
    }

    searchActivities = async () => {
        const {filters} = this.state;
        const activities = await this.props.getActivities(filters);

        this.setState({
            items: activities,
        })
    }

    loadMore = async () => {
        const {filters, items} = this.state;
        filters.offset += 5;

        const activities = await this.props.getActivities(filters);

        this.setState({
            filters: filters,
            items: items.concat(activities),
        });
    }

    handleChange = (field, value) => {
        this.setState({filters: {[field]: value}});
    }

    render() {
        const {items} = this.state;

        return <Container flow={'column'}>
            <Container>
                <LinkText onClick={() => this.props.changeLocation('/')}>&#8592; Назад</LinkText>
            </Container>
            <Container margin={'24px 0 0 0'}>
                <Input
                    width={'100%'}
                    padding={'8px'}
                    margin={'0 24px 0 0'}
                    placeholder={'Направление или номер группы'}
                    onChange={(e) => this.handleChange('query', e.currentTarget.value)}
                />
                <Button onClick={() => this.searchActivities()}>Найти</Button>
            </Container>
            <StyledContainer flow={'column'} margin={'24px 0 0 0'}>
                <Container>
                    <Text opacity={0.5}>{'Найдено ' + items.length + ' групп'}</Text>
                </Container>
                <Container margin={'24px 0 0 0'} flow={'column'}>
                    {items && items.map((item, i) => <ItemContainer key={'list-item_' + i} flow={'column'} width={'100%'}>
                        <Container flow={'column'}>
                            {item.address && <SubTitle>{'По адресу ' + item.address}</SubTitle>}
                            <Title>{item.title}</Title>
                            <Description>{item.description}</Description>
                        </Container>
                        <GroupList>
                            {item.groups.map((group, ii) => <GroupItem flow={'column'} key={'list-item_' + i + '_group_' + ii}>
                                <GroupStatus>{group.status}</GroupStatus>
                                <Container flow={'row'} justify={'flex-start'}>
                                    <GroupId>Группа</GroupId>
                                    <GroupIdNumber>{group.name}</GroupIdNumber>
                                </Container>
                                <Container flow={'column'}>
                                    {group.times && group.times.map((tm, j) => <Container
                                        key={'list-item_' + i + '_group_' + ii + '_tm_' + j}>
                                        <Text>{tm}</Text>
                                    </Container>)}
                                </Container>
                            </GroupItem>)}
                        </GroupList>
                        <Container margin={'24px 0 0 0'}>
                            <GreenButton>Узнать больше</GreenButton>
                        </Container>
                        <Separator />
                    </ItemContainer>)}
                    <Container margin={'16px 0 0 0'}>
                        {items.length > 0 && <ShowMoreButton onClick={this.loadMore}>Показать еще</ShowMoreButton>}
                    </Container>
                </Container>
            </StyledContainer>
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
    }
}

const mapDispatchToProps = {
    push,
    getActivities
}

export const Activities = connect(mapStateToProps, mapDispatchToProps)(SearchComponent);
