import React, {Component} from "react";
import {push} from "connected-react-router";
import styled from "styled-components";
import {connect} from "react-redux";
import {Container} from "../components/atoms/container";
import Header from "../components/header";
import {Activities} from "./activities";
import {Questions} from "./questions";
import {Recommends} from "./recommends";

const StyledContainer = styled(Container)`

`

class MainComponentClass extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: this.props.page,
        };
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {page} = this.props;
        if (prevProps.page !== page) {
            this.setState({
                page: page,
            })
        }
    }

    getContent = () => {
        const {page} = this.state;

        switch (page && page.split('?')[0]) {
            case '/questions':
                return <Questions
                    changeLocation={this.changeLocation}
                />
            case '/list':
                return <Activities
                    changeLocation={this.changeLocation}
                />
            default:
                return <Recommends
                    changeLocation={this.changeLocation}
                />
        }
    }

    changeLocation = (loc) => {
        this.props.push(loc)
        this.setState({
            page: loc,
        })
    }

    render() {
        return <Container flow={'column'} margin={'0 0 10vh 0'}>

            <Header />
            <StyledContainer flow={'column'}>
                {this.getContent()}
            </StyledContainer>
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
        isLoading: state.main.isLoading,
    }
}

const mapDispatchToProps = {
    push,
}

export const MainComponent = connect(mapStateToProps, mapDispatchToProps)(MainComponentClass);