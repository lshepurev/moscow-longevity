import React, {Component} from "react";
import {connect} from "react-redux";
import styled from "styled-components";
import {push} from "connected-react-router";
import {Container} from "../components/atoms/container";
import {Text} from "../components/atoms/text";
import {Button} from "../components/atoms/button";
import {LinkText} from "../components/atoms/link-text";
import {getRecommends} from "../actions/service";

const StyledTitle = styled(Text)`
    font-weight: 600;
    font-size: 18px;
`

const StyledItemContainer = styled(Container)`
    border: 1px Solid #dfe1e6;
    width: auto;
`

class RecommendsComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            recommends: [],
        }
    }

    async componentDidMount() {
        const activities = await this.props.getRecommends();

        this.setState({recommends: activities})
    }

    render() {
        const {recommends} = this.state;

        return <Container flow={'column'}>
            <Container flow={'column'}>
                <Container>
                    <StyledTitle>Самые популярные занятия</StyledTitle>
                </Container>
                <Container margin={'16px 0 0 0'} minHeight={'48px'}>{
                    recommends && recommends.map((item, i) => <StyledItemContainer key={'rec-' + i} flow={'column'} padding={'16px'} margin={'0px 16px 0 0'}>
                        <Container>
                            <Text whiteSpace={'nowrap'}>{item.title}</Text>
                        </Container>
                        <Container flow={'column'} margin={'16px 0 0 0'}>
                            {item.items.map((course, i) => <Container key={'rec-course_' + i}>
                                <LinkText>{course.name}</LinkText>
                            </Container>)}
                        </Container>
                    </StyledItemContainer>)
                }</Container>
            </Container>
            <Container flow={'column'} margin={'24px 0 0 0'}>
                <StyledTitle>Поможем вам подобрать интересные занятия</StyledTitle>
                <Text>Пройдите тест и узнайте, какие занятия подходят именно вам</Text>
            </Container>
            <Container margin={'24px 0 0 0'}>
                <Button onClick={() => this.props.changeLocation('/questions')} margin={'0 16px 0 0'}>Подобрать занятие</Button>
                <Button onClick={() => this.props.changeLocation('/list')}>Выбрать из списка</Button>
            </Container>
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
    }
}

const mapDispatchToProps = {
    push,
    getRecommends,
}

export const Recommends = connect(mapStateToProps, mapDispatchToProps)(RecommendsComponent)