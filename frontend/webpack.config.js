const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const Dotenv = require('dotenv-webpack');

const BUILD_DIR = path.resolve(__dirname, 'build')
const SRC_DIR = path.resolve(__dirname, 'src')
const MODE = process.env.NODE_ENV || 'development'

module.exports = {
    entry: {
        index: ['babel-polyfill', SRC_DIR + '/index.js']
    },
    output: {
        path: BUILD_DIR,
        filename: '[name].[hash].bundle.js',
        publicPath: '/'
    },
    watchOptions: {
        poll: true
    },
    devtool: false,
    devServer: {
        static: {
            directory: BUILD_DIR
        },
        compress: true,
        hot: true,
        open: true,
        historyApiFallback: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            },
            {
                test: /\.(png|jpg|jpeg|gif|ico|svg$)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: './img/[name].[hash].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    optimization: {
        moduleIds: 'named'
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin(
            {
                inject: true,
                template: './public/index.html',
                favicon: './public/img/favicon.ico'
            }
        ),
        new CopyWebpackPlugin({
                patterns: [
                    {from: './public/img', to: 'img/[name][ext]'},
                ],
            },
        ),
        new Dotenv({
            path: './.env',
            safe: true,
            systemvars: true
        }),
        new webpack.EnvironmentPlugin({
            NODE_ENV: MODE,
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV' : JSON.stringify(MODE)
        })
    ]
}
