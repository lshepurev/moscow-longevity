import os
import pandas as pd
from geopy.geocoders import Nominatim
from geopy.distance import geodesic as GD

from src.resources import DATA_PATH


class GeoTool(object):
    def __init__(self):
        self.users_df = pd.read_csv(os.path.join(DATA_PATH, 'raw', 'users.csv'))
        self.groups_df = pd.read_csv(os.path.join(DATA_PATH, 'raw', 'groups.csv'))
        self.geolocator = Nominatim(user_agent="my_app")

    def get_address_coordinates(self, address):    
        address = self.normalize_address(address)
        
        location = self.geolocator.geocode(address)
        if location is not None:
            latitude = location.latitude
            longitude = location.longitude
    
            return latitude, longitude
        else:
            return None
    
    @staticmethod
    def get_points_distance(point_1, point_2):
        if point_1 is not None and point_2 is not None:
            return int(GD(point_1, point_2).meters)
        else:
            return None

    @staticmethod
    def address_deduplication(address):
        address = address.lower()
        if address.count('москва') > 1 or address.count('округ') > 1:
            address = address[:int(len(address)/2)-1]
        return address

    def normalize_address(self, address):
        address = address.lower()
        address = self.address_deduplication(address)

        if 'корпус' in address or 'к.' in address:
            address = ', '.join(address.split(',')[:-1])
        
        for i in ['г.', 'город', 'ул.', 'улица', 'дом', 'д.']:
            address = address.replace(i, '')
        
        address = address.replace('муниципальный округ', 'москва,')
    
        return address

    def get_user_coordinates(self, user_id):
        if user_id in list(self.users_df['уникальный номер']):
            address = self.users_df[self.users_df['уникальный номер'] == int(user_id)].iloc[0]['адрес проживания']
            coordinates = self.get_address_coordinates(address)
            return coordinates
        return None

    def get_group_coordinates(self, group_id):
        if group_id in list(self.groups_df['уникальный номер']):
            row = self.groups_df[self.groups_df['уникальный номер'] == int(group_id)].iloc[0]

            for col in ['адрес площадки', 'район площадки']:
                address = row[col]
                coordinates = self.get_address_coordinates(address)
                if coordinates is not None:
                    return coordinates
        return None

    def get_user_group_distance(self, user_id, group_id):
        point_1 = self.get_user_coordinates(user_id)
        point_2 = self.get_group_coordinates(group_id)
        res = self.get_points_distance(point_1, point_2)
        return res
