from flask import request

from src.service.service_resources import app, request_handler, N_TOP_DEFAULT


@app.route('/predict_activities', methods=["GET"])
def predict_activities():
    request_dict = request.get_json()
    response_dict = dict()

    try:
        user_id = int(request_dict['user_id'])
        n_top = int(request_dict.get('n_top', N_TOP_DEFAULT))
        activity_type = request_dict.get('activity_type', None)

        activities_id_list = request_handler.predict_activities(user_id, activity_type, n_top)
        response_dict['activities_id'] = activities_id_list

    except Exception as e:
        response_dict['error'] = str(e)

    return response_dict


@app.route('/predict_groups', methods=["GET"])
def predict_groups():
    request_dict = request.get_json()
    response_dict = dict()

    try:
        user_id = int(request_dict['user_id'])
        n_top = int(request_dict.get('n_top', N_TOP_DEFAULT))

        groups_id_list = request_handler.predict_groups(user_id, n_top)
        response_dict['groups_id'] = groups_id_list

    except Exception as e:
        response_dict['error'] = str(e)

    return response_dict


@app.route('/user_group_distance', methods=["GET"])
def user_group_distance():
    request_dict = request.get_json()
    response_dict = dict()

    try:
        user_id = int(request_dict['user_id'])
        group_id = int(request_dict['group_id'])

        distance_res = request_handler.get_user_group_distance(user_id, group_id)
        response_dict['distance'] = distance_res

    except Exception as e:
        response_dict['error'] = str(e)

    return response_dict


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=False)
