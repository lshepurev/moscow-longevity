from flask import Flask

from src.service.request_handler import RequestHandler
from src.models.svdpp_activities import SVDppActivitiesRecsys
from src.models.svdpp_groups import SVDppGroupsRecsys
from src.geo_tool import GeoTool

N_TOP_DEFAULT = 10

app = Flask(__name__)

activities_model = SVDppActivitiesRecsys()
groups_model = SVDppGroupsRecsys()

geo_tool = GeoTool()

request_handler = RequestHandler(activities_model, groups_model, geo_tool)
