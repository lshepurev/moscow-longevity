
class RequestHandler(object):
    def __init__(self, activities_model, groups_model, geo_tool):
        self.activities_model = activities_model
        self.groups_model = groups_model
        self.geo_tool = geo_tool

    def predict_activities(self, user_id, activity_type, n_top):
        if activity_type not in [None, 'Для ума', 'Для души', 'Для тела']:
            raise Exception('Недопустимое значение activity_type')

        proba_dict = self.activities_model.predict_proba(user_id, activity_type=activity_type)
        activities_id_list = [activity_id for activity_id, proba in sorted(proba_dict.items(),
                                                                           key=lambda item: -item[1])]
        return activities_id_list[:n_top]

    def predict_groups(self, user_id, n_top):

        proba_dict = self.groups_model.predict_proba(user_id)
        groups_id_list = [group_id for group_id, proba in sorted(proba_dict.items(),
                                                                 key=lambda item: -item[1])]
        return groups_id_list[:n_top]

    def get_user_group_distance(self, user_id, group_id):
        return self.geo_tool.get_user_group_distance(user_id, group_id)
